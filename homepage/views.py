from django.shortcuts import render, render_to_response
from django.template import RequestContext

def index(request):
    return render_to_response('homepage/index_cn.html', {}, context_instance=RequestContext(request))

def index_en(request):
    return render_to_response('homepage/index_en.html', {}, context_instance=RequestContext(request))